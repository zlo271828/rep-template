  module.exports = {
  extends: 'airbnb',
  parser: "babel-eslint",
            rules: {
              'arrow-parens': [
                2,
                'as-needed'
              ],
              semi: [
                2,
                'never'
              ]
            }
    };
