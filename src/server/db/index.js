//
//  All database operations
//

import * as common from './common'
import * as users from './users'
import * as session from './session'
import * as query from './query'

export default Object.assign({},
  common, users, session, query)
