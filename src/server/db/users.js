//
//  User data recording
//
import crypto from 'crypto'
import config from 'config'
import { pool, dbFailure } from './common'

// Return user id by their email,
// null on stale or broken email
export async function userIdByEmail(email) {
  return pool.query(`
    select "user_id"
    from "user"
    where "email" = $1
    limit 1
    `,
  [email]).then(res => {
    if (res.rowCount < 1) {
      return null
    }

    const uid = res.rows[0].user_id
    return uid
  })
}

// Create an empty user record and return its id
export const makeUser = async (email, password) => {
  const client = await pool.connect()
  let hashPassword

  if (password !== undefined) {
    if (password.length < 4) {
      throw new Error('The password must contain more than 4 characters')
    }
  }

  const salt = crypto.randomBytes(config.crypto.hash.length).toString('base64')

  if (password) {
    hashPassword = crypto.pbkdf2Sync(
      password,
      salt,
      12000,
      config.crypto.hash.length,
      'sha256',
    ).toString('base64')
  } else {
    throw new Error('Error while generating a password')
  }

  try {
    await client.query('begin')

    const user = await userIdByEmail(email)

    if (user !== null) {
      await client.query('rollback')
      client.release()
      throw new Error('User already exists')
    }
    await client.query(`
      insert into "user" (email, salt, digest) values ($1, $2, $3)
      `, [email, salt, hashPassword])

    await client.query('commit')
  } catch (e) {
    await client.query('rollback')
    client.release()
    dbFailure(e)
  }
}

// Return user id by their session token,
// null on stale or broken token
export async function userIdBySessionToken(token) {
  return pool.query(`
    select "user_id"
    from "session"
    where "token" = $1
    limit 1
    `,
  [token]).then(res => {
    if (res.rowCount < 1) {
      return null
    }

    const uid = res.rows[0].user_id
    return uid
  })
}

export async function checkPassword(id, password) {
  const credentials = await pool.query(`
    select "digest", "salt"
    from "user"
    where "user_id" = $1
    limit 1
    `, [id])

  const { digest, salt } = credentials.rows[0]
  if (!password) return false
  if (!digest) return false
  return crypto.pbkdf2Sync(
    password,
    salt,
    12000,
    config.crypto.hash.length,
    'sha256',
  ).toString('base64') === digest
}

// Return all available info about this user
export async function getUser(uid) {
  const res = await pool.query(`
    select
      "user"."user_id",
      "email"
    from "user"
    where "user_id" = $1
    limit 1
    `,
  [uid])
  if (res.rowCount < 1) {
    return null
  }

  const fetch = res.rows[0]
  return {
    id: fetch.user_id,
  }
}

// export const mergeUsers = async function ({ keepUid, removeUid }) {
//   const client = await pool.connect()
//   try {
//     await client.query('begin')
//     // eslint-disable-next-line no-restricted-syntax
//     for (const table of ['user']) {
//       // eslint-disable-next-line no-await-in-loop
//       await client.query(`
//       update "${table}" set "user_id" = $1 where "user_id" = $2
//       `,
//       [keepUid, removeUid])
//     }
//
// We want to pretend that all sessions on behalf of merged-from user belong to merged-into user
//     const replacedSessionData = { passport: { user: keepUid } }
//
//     await client.query(`
//     update "session"
//     set
//     "user_id" = $1,
//     "associated_data" = "associated_data" || $3::jsonb
//     where "user_id" = $2
//     `,
//     [keepUid, removeUid, JSON.stringify(replacedSessionData)])
//     await client.query(`
//     delete from "user" where "user_id" = $1
//     `,
//     [removeUid])
//     await client.query('commit')
//   } catch (e) {
//     await client.query('rollback')
//     dbFailure(e)
//   }
//   client.release()
//
//   return keepUid
// }
