import passport from 'passport'
import LocalStrategy from 'passport-local'
import db from '../../db'
import logger from '../../utils/logger'

const log = logger('handlers/auth/passport')

passport.use('local', new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password',
  passReqToCallback: true,
  session: false,
},
// eslint-disable-next-line consistent-return
(async (req, email, password, done) => {
  let id
  try {
    id = await db.userIdByEmail(email)
  } catch (err) {
    log.error(err)
    return done(err)
  }
  const passwordCheckFlag = await db.checkPassword(id, password)
  if (!id || !passwordCheckFlag) {
    log.error('User does not exist, or invalid password')
    return done(null, false)
  }
  // log.info(`User: ${email} was founded!`)
  done(null, id)
})))

passport.serializeUser((user, done) => {
  done(null, user)
})

passport.deserializeUser((uid, done) => {
  done(null, uid)
})
