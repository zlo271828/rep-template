import Router from 'express-promise-router'
import passport from 'passport'
import db from '../../db'
import logger from '../../utils/logger'
import './passport'

const router = new Router()
const log = logger('handlers/auth')

router.post('/auth/login', passport.authenticate('local'), (req, res) => {
  if (req.user) return res.status(200).end('Success!')
  return res.status(404).end('Auth error!')
})

router.post('/auth/registration', async (req, res) => {
  const { email, password } = req.body
  if (email === undefined || password === undefined) return res.status(404).end('Registration error!')
  try {
    await db.makeUser(email, password)
    return res.status(200).end('Success!')
  } catch (e) {
    log.error('Error while creating a user\n', e)
  }
  return res.status(404).end('Registration error!')
})

router.get('/deauth', (req, res) => {
  req.logout()
  res.redirect('/')
})

export default router
