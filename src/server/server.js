import express from 'express'
import * as applyMiddleware from './middlewares'

const app = express()
applyMiddleware.default(app)

export default app
