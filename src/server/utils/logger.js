import log4js from 'log4js'
import config from 'config'

const logger = name => {
  const log = log4js.getLogger(name)
  log.level = config.logLevel
  return log
}

export default logger
