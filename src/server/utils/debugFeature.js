/* eslint-disable global-require */
if (process.env.TRACE) {
  Error.stackTraceLimit = 5
  require('trace')
  require('clarify')
}
