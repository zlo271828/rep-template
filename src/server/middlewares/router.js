import Router from 'express-promise-router'
import auth from '../handlers/auth'

const router = new Router()

// Auth logic
router.use(auth)

export default router
