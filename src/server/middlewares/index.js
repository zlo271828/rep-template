import path from 'path'
import helmet from 'helmet'
import cors from 'cors'
import logger from 'morgan'
import config from 'config'
import bodyParser from 'body-parser'
import glob from 'glob'
import cookieParser from 'cookie-parser'
import expressSession from 'express-session'
import passport from 'passport'
import log4 from '../utils/logger'
import db from '../db'

const log = log4('middleware/index')
const cookieSecret = process.env.COOKIE_SECRET
const SessionStore = db.createSessionStore(expressSession)
const sessionStore = new SessionStore({ pool: db.pool })
const cookie = cookieParser(cookieSecret)
const session = expressSession({
  store: sessionStore,
  name: 'antihype',
  secret: cookieSecret,
  resave: true,
  saveUninitialized: true,
})

const applyMiddleware = (app, ...middleware) => {
  middleware.forEach(m => app.use(m))
}

export default app => {
  const middleware = glob
    .sync(path.join(__dirname, '/*.js'))
    .filter(m => m.indexOf('index.js') === -1)
    .map(m => m.replace(path.extname(m), ''))
  log.info(middleware.map(m => `middleware injected ${path.basename(m)}`))
  applyMiddleware(
    app,
    cookie,
    session,
    passport.initialize(),
    passport.session(),
    helmet(),
    cors(),
    logger(config.restLogLevel),
    bodyParser.json(),
    bodyParser.urlencoded({ extended: false }),
    /* eslint-disable-next-line */
    ...middleware.map(m => require(m).default),
  )
}
