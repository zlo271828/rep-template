--
-- Feed this to a fresh installation of Postgres
--

create extension if not exists "uuid-ossp";
create extension if not exists "pgcrypto";

create table "user" (
  "user_id" uuid primary key not null default uuid_generate_v4(),
  "email" character varying(256) unique not null,
   "salt" character varying(256) not null,
  "digest" character varying(256) not null
);

create table "session" (
  "user_id" uuid null references "user"("user_id"),
  "token" character varying(48) primary key not null default uuid_generate_v4(),
  "timestamp" timestamp without time zone not null default now(),
  "associated_data" jsonb not null default '{}'::jsonb
);
create function "session_expire"() returns trigger language plpgsql as $$
begin
  delete from "session" where "timestamp" < now() - interval '1 week';
  return new;
end
$$;
create trigger "session_expire_trigger" after insert on "session" execute procedure "session_expire"();
create function "session_refresh"() returns trigger language plpgsql as $$
begin
  new."timestamp" := now();
  return new;
end
$$;
create trigger "session_refresh_trigger" before update on "session" for each row execute procedure "session_refresh"();
