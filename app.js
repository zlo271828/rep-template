require('babel-register')
require('dotenv').config()
require('./src/server/utils/debugFeature')
const http = require('http')
const config = require('config')
const app = require('./src/server/server.js')
const logger = require('./src/server/utils/logger')
const db = require('./src/server/db').default

const port = config.port || 8080
const server = http.createServer(app.default)
const log = logger.default('app')

db.check(error => {
  if (error != null) {
    log.error(error)
    process.exit(3)
  }

  server.listen(port, () => {
    log.info(`Server listen on port: ${port}`)
  })
})
